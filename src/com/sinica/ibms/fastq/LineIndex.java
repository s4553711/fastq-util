package com.sinica.ibms.fastq;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

public class LineIndex {
	
	private static boolean isBufferEnd(int nRead, int i) {
		return i + 1 == nRead ? true : false;
	}
	
    public static void CreateIndex(InputStream in, int interval) {
        BufferedInputStream bis = null;
        int NL = 0;
        int fastqCount = 0;
        long total_in = 0;
        try {
            try {
                byte[] data = new byte[1048576];
                int nRead = 0;
                bis = new BufferedInputStream(in);
                while ((nRead = bis.read(data)) != -1) {
                    int i = 0;
                    while (i < nRead) {
                    	if (data[i] == 10) {
                    		NL++;
                    		//System.out.println("1> "+fastqCount + "\t" + (total_in + i + 1) + "\t" + i + " / + " + nRead + ", NL: " +NL+ ", Next: " + data[i+1]);
                    		if (( isBufferEnd(nRead, i) || data[i + 1] == 64 || data[i + 1] == 0) && NL % 4 == 0) {
                    			fastqCount++;
                                if (fastqCount % interval == 0) {
                                    System.out.println(fastqCount + "\t" + (total_in + i + 1));
                                }                   			
                                NL = 0;
                    		}
                    	}
                    	i++;
                    }
                    total_in += nRead;
                }
            }
            catch (IOException e) {
                e.printStackTrace();
                try {
                    bis.close();
                }
                catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        finally {
            try {
                bis.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
