package com.sinica.ibms;

import com.sinica.ibms.fastq.LineIndex;

public class CreateIndex {
	public static void main(String args[]) {
		if (args.length == 0) {
			System.err.println("Usage: CreateIndex interval]");
		}
		LineIndex.CreateIndex(System.in, Integer.valueOf(args[0]));
	}
}
